###### Build

`mvn clean install`

###### Running the encryption utility

Running from Jar

Encrypt

`java -jar enc-1.0.0-jar-with-dependencies.jar -m encrypt -p hello
`
Decrypt 

`java -jar enc-1.0.0-jar-with-dependencies.jar -m decrypt -p 1vUORjk9yJU=
`

###### Generate executable binary for Linux Systems

`cat enc.sh enc-1.0.0-jar-with-dependencies.jar > enc
`

Use the `enc` binary file as regular Linux command

Encrypt

`bash enc -m encrypt -p hello`

Decrypt 

`bash enc -m decrypt -p 1vUORjk9yJU=
`

