package com.appnomic.appsone;

public enum EncryptionMode {
    ENCRYPT,
    DECRYPT;

    private EncryptionMode() {
    }

    public static EncryptionMode getMode(String arg) {
        return valueOf(arg.toUpperCase());
    }
}