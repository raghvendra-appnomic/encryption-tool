package com.appnomic.appsone;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        log.info("====AppsOne Encryption-Decryption Tool====");

        try {
            EncryptionArgs encryptionArgs = parseArguments(args);
            PasswordConvertor passwordConvertor = new PasswordConvertor();

            if (encryptionArgs.getMode().equals(EncryptionMode.ENCRYPT)) {
                String encryptedPassword = passwordConvertor.getEncryptedPassword(encryptionArgs.getPassword());
                log.info("Password after encryption: {}", encryptedPassword);
            }

            if (encryptionArgs.getMode().equals(EncryptionMode.DECRYPT)) {
                String decryptedPassword = passwordConvertor.getDecryptedPassword(encryptionArgs.getPassword());
                log.info("Password after decryption: {}", decryptedPassword);
            }
        } catch (Exception e) {
            log.error("Error encrypting/decrypting password: {}" + e.getMessage());
        }
    }

    private static EncryptionArgs parseArguments(String[] args) throws Exception {

        CommandLineParser parser = new DefaultParser();
        Options options = new Options();

        Option inputPassword = new Option("p", "password", true, "password to encrypt/decrypt");
        inputPassword.setRequired(true);
        options.addOption(inputPassword);

        Option modeInput = new Option("m", "mode", true, "Mode of operation - Encryption or Decryption");
        modeInput.setRequired(true);
        options.addOption(modeInput);

        CommandLine cmd = parser.parse(options, args);

        EncryptionMode mode = EncryptionMode.getMode(cmd.getOptionValue("mode"));
        String password = cmd.getOptionValue("password");

        EncryptionArgs encryptionArgs = new EncryptionArgs();

        if (EncryptionMode.ENCRYPT.equals(mode)) {
            encryptionArgs.setMode(EncryptionMode.ENCRYPT);
            if (StringUtils.isEmpty(password)) {
                log.error("Invalid or Empty password to encrypt: {}", password);
                throw new Exception("Invalid or Empty password to encrypt: " + password);
            }
            log.info("Password before encryption: {}", password);
            encryptionArgs.setPassword(password);
        } else if (EncryptionMode.DECRYPT.equals(mode)) {
            encryptionArgs.setMode(EncryptionMode.DECRYPT);
            if (StringUtils.isEmpty(password)) {
                log.error("Invalid or Empty password to encrypt: {}", password);
                throw new Exception("Invalid or Empty password to encrypt: " + password);
            }
            encryptionArgs.setPassword(password);
            log.info("Password before decryption: {}", password);

        } else {
            log.error("Invalid Mode for encryption-decryption: {} ", mode);
            throw new Exception("Invalid Mode: " + mode);
        }

        return encryptionArgs;
    }
}