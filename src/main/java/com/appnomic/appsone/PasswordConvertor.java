package com.appnomic.appsone;

import org.apache.log4j.Logger;

public class PasswordConvertor
{

    private static final String encryptionKey = "123456789 vitage@123 key";
    private static String encryptionScheme = "DESede";
    private EncryptionLogic encrypter = null;
    protected Logger log = Logger.getLogger(PasswordConvertor.class);


    public PasswordConvertor()
    {
        try
        {
            encrypter = new EncryptionLogic(encryptionScheme, encryptionKey);
        }
        catch (Exception e)
        {
            log.error("Error in PasswordConvertor..", e);
        }
    }

    public String getEncryptedPassword(String password)
    {
        return encrypter.encrypt(password);
    }

    public String getDecryptedPassword(String password)
    {
        return encrypter.decrypt(password);
    }
}