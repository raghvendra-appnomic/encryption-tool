package com.appnomic.appsone;

public class EncryptionArgs {
    private EncryptionMode mode;
    private String password;

    public EncryptionArgs() {
    }

    public EncryptionMode getMode() {
        return mode;
    }

    public void setMode(EncryptionMode mode) {
        this.mode = mode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DiscoveryAgentArgs{" +
                "mode=" + mode +
                ", password='" + password + '\'' +
                '}';
    }
}
