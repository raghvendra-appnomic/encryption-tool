package com.appnomic.appsone;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.spec.KeySpec;


public class EncryptionLogic
{
    private KeySpec keySpec;
    private SecretKeyFactory keyFactory;
    private Cipher cipher;

    private static final String UNICODE_FORMAT = "UTF8";
    protected Logger log = Logger.getLogger(EncryptionLogic.class);

    public EncryptionLogic(String encryptionScheme, String encryptionKey)
    {
        if (encryptionKey == null || encryptionKey.trim().length() < 24)
        {
            log.error("Encryption key was not valid");
        }
        else
        {
            try
            {
                byte[] keyAsBytes = encryptionKey.getBytes(UNICODE_FORMAT);
                if (encryptionScheme.equals("DESede"))
                {
                    keySpec = new DESedeKeySpec(keyAsBytes);
                    keyFactory = SecretKeyFactory.getInstance(encryptionScheme);
                    cipher = Cipher.getInstance(encryptionScheme);
                }
            }
            catch (Exception e)
            {
                log.error("Encryption logic is not completed..", e);
            }
        }
    }

    public String encrypt(String stringToEncrypt)
    {
        if (stringToEncrypt == null || stringToEncrypt.trim().length() == 0)
        {
            log.warn("String was null or empty, can't be encrypted.");
            return stringToEncrypt;
        }
        else
        {
            try
            {
                Base64 base64encoder = new Base64();
                SecretKey key = keyFactory.generateSecret(keySpec);
                cipher.init(Cipher.ENCRYPT_MODE, key);
                byte[] cleartext = stringToEncrypt.getBytes(UNICODE_FORMAT);
                byte[] ciphertext = cipher.doFinal(cleartext);
                //BASE64Encoder encodes a binary stream as a "Base64" format string.
                //BASE64Decoder decodes a "Base64" format string into a binary stream
                return new String(base64encoder.encode(ciphertext));
            }
            catch (Exception e)
            {
                log.error("Encryption is not completed..returning same string..", e);
                return stringToEncrypt;
            }
        }
    }

    public String decrypt(String stringToDecrypt)
    {
        if (stringToDecrypt == null || stringToDecrypt.trim().length() < 8)
        {
            //log.error( "String was not in encrypted format, so returning same string.." );
            return stringToDecrypt;
        }
        else
        {
            try
            {
                Base64 base64decoder = new Base64();
                SecretKey key = keyFactory.generateSecret(keySpec);
                cipher.init(Cipher.DECRYPT_MODE, key);
                byte[] cleartext = base64decoder.decode(StringUtils.getBytesUtf8(stringToDecrypt));
                byte[] ciphertext = cipher.doFinal(cleartext);
                //BASE64Encoder encodes a binary stream as a "Base64" format string.
                //BASE64Decoder decodes a "Base64" format string into a binary stream
                return bytes2String(ciphertext);
            }
            catch (Exception e)
            {
                log.warn("Decryption is not completed, so returning same string.." + e.getMessage());
                return stringToDecrypt;
            }
        }

    }

    public static String bytes2String(byte[] bytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            stringBuffer.append((char) bytes[i]);
        }
        return stringBuffer.toString();
    }

}